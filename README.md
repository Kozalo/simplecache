Simple Cache
==============================

Для русскоговорящих
-------------------

Данный класс представляет собой реализацию простейшего файлового кэша. Вам предоставляется объект, в поля которого
можно записывать любые сериализуемые данные.  
Как им пользоваться?  

```php
<?php
use ru\kozalo\SimpleCache;

// Создаём объект кэша. В качестве параметра конструктор принимает любую строку, которая станет названием файла для кэша.
// Таким образом можно создавать несколько кэшей с различными наборами данных.
$cache = new SimpleCache('my_cache');

// Магия позволяет работать с данными просто, как полями объекта.
$cache->someData = "Очень важная строка";
echo $cache->someData;
```

Если взглянуть внутрь класса, то можно заметить, что у конструктора есть ещё два параметра.
Второй отвечает за отключение автоматического отражения изменений объекта на реальный файл кэша на диске.
А третий указывает UNIX-разрешения для папки кэша.  
Вы можете спросить: "Какой ещё папки, если раньше речь шла только о файлах?" Все файлы кэша аккуратно складываются
в специально выделенную для них папку. По умолчанию для этого используется папка *cache* в корне сайта. Переопределить
же её можно, создав в том же корне файл *env.cfg*, который представляет собой простой PHP-файл, возвращающий массив
с ключом `cache`, значением которого также является массив, но с ключом `path`, значением которого, собственно, и задаётся
путь для папки кэша. Файл с примером (*env.cfg.example*) лежит в корне репозитория.  

```php
<?php
use ru\kozalo\SimpleCache;

// Отлючим автоматическое сохранение кэша на диск и разрешим полный доступ к папке только владельцу.
// Впрочем, если папка уже создана, параметр `$folderPermissions` не имеет вообще никакой силы.
$cache = new SimpleCache('my_cache', false, 0700);

$cache->calc1 = pi() * 100 + 123456;
$cache->calc2 = pi()^10 - 98765;
$cache->calc3 = log(pi() / 3) * 0.456;

// А теперь вручную сохраним результаты наших вычислений в файл кэша.
$cache->save();
```

Теперь разберём один из основных моментов кэша: проверку устаревания данных. Этим вы должны заниматься самостоятельно,
но кэш автоматически сохраняет текущий *timestamp* для каждого заносимого в него поля. Рассмотрим пример:  

```php
<?php
use ru\kozalo\SimpleCache;

$cache = new SimpleCache('my_cache');

// Будем считать данные, загруженные раньше, чем 12 часов назад от текущего момента, устаревшими.
$obsolescenceDate = strtotime('-12 hours');

// Метод `getDate()` возвращает *timestamp* со временем, когда данные были занесены в кэш.
// Обнаружив, что данные устарели, обновляем их.
if ($cache->getDate('someData') < $obsolescenceDate)
    $cache->someData = "Обновлённая, но всё ещё важная строка";
```

Что делать, если в коде уже активно используется кэш, но нужно срочно его сбросить, чтобы загрузились новые данные?
Или может по определённому условию весь кэш становится неверным и его можно полностью очищать? Для этого есть метод
`invalidate()`, который удаляет файл кэша с диска и очищает его представление в объекте.  
А если для отладки или чего-либо ещё понадобилось вообще временно отключить кэш? Просто добавьте после создания объекта
вызов метода `disable()`. После этого вызовы всех методов будут возвращать *null* или *false*. Для обратного включения
есть симметричный метод `enable()`.


In English
----------

This class is just a simple file-based cache. You have an object to put your serializable data into.  
Let's consider how to use the class using the example below.  

```php
<?php
use ru\kozalo\SimpleCache;

// Firstly, we should create an instance of the cache. The constructor takes a string which will be used as the name of a file.
// Due to this parameter, we may use several caches with different sets of data at the same time.
$cache = new SimpleCache('my_cache');

// Magic lets us get and set data very easily.
$cache->someData = "A very important string";
echo $cache->someData;
```

Actually, if you look at the code, you'll see that the constructor takes other two parameters.  
By default, each time when you change the value of a field, the library synchronizes the object and the file behind it.
It may be much more efficiently to disable this feature and dump the cache to the file manually.  
The last parameter let you change UNIX permissions for a cache folder.  
"Hold on! What's the folder?" — you may ask me. Well, all cache files are stored inside of a special directory.
By default, it's the *cache* folder in the root directory of your website. To change this behavior, place a PHP file
named "env.cfg" at the root. This file must return an array with a key "cache". And the value of this key must be
an array with a key "path". There is an example: file `env.cfg.example` alongside with this manual.  

```php
<?php
use ru\kozalo\SimpleCache;

// Let's disable auto synchronization and give full access to the owner only.
// Note that permissions only take effect if the folder doesn't exist yet.
$cache = new SimpleCache('my_cache', false, 0700);

$cache->calc1 = pi() * 100 + 123456;
$cache->calc2 = pi()^10 - 98765;
$cache->calc3 = log(pi() / 3) * 0.456;

// Now we have to save our calculations to the file manually.
$cache->save();
```

What about data aging? How can we tell the library to update the data? Nohow. It depends on you. Let me show an example.  

```php
<?php
use ru\kozalo\SimpleCache;

$cache = new SimpleCache('my_cache');

// Let's consider all data older than 12 hours as obsolete.
$obsolescenceDate = strtotime('-12 hours');

// `getDate()` returns a *timestamp*, which represents the time when a data was put into the cache.
// Finding out that the data is too old, we should update it.
if ($cache->getDate('someData') < $obsolescenceDate)
    $cache->someData = "An updated but still important string";
```

OK. What if we want to update entire cache? Or just clear it from all data? Use the method `invalidate()`. It deletes the cache file
and make the cache object empty.  
What if we need to disable cache temporary for debugging purposes? Use the method `disable()`. After you call it,
all other methods will return *null* and *false* values only. To make the cache work again, use the symmetric method  `enable()`.
