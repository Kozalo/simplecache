<?php
namespace ru\kozalo;

use RuntimeException;
use stdClass;


/**
 * Class SimpleCache
 *
 * This class let you take advantage of a simple caching system.
 * By default, it stores files inside of the "cache" folder in the root directory of you website. To change this behavior, place a PHP file named "env.cfg" at the root.
 * This file must return an array with a key "cache". The value of the key must be an array with a key "path".
 *
 * @author Leonid Kozarin <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2017
 * @package ru\kozalo
 * @license MIT
 */
class SimpleCache
{
    private $cacheFile;         // Path to a cache file.
    private $cacheContent;      // A cache object containing the content of the file.
    private $autosave = true;   // If it's true, the cache file will be updated after each modification of the cache object.
    private $disabled = false;  // If it's true, all methods return *null* and *false* values.


    /**
     * SimpleCache constructor.
     *
     * @param string $name A name for your cache file.
     * @param bool $autosave If it's true (by default), the cache file will be updated after each modification of the cache object. Otherwise, you must call the method `save()` manually.
     * @param int $folderPermissions If the cache folder is not found, it will be created with these permissions (0770 by default).
     */
    public function __construct($name, $autosave = true, $folderPermissions = 0770)
    {
        $this->autosave = $autosave;

        $cachePath = 'cache';

        if (file_exists('env.cfg')) {
            $env = include('env.cfg');
            if (array_key_exists('cache', $env) && array_key_exists('path', $env['cache']))
                $cachePath = $env['cache']['path'];
        }

        if (!is_dir($cachePath))
            mkdir($cachePath, $folderPermissions);

        $this->cacheFile = "$cachePath/$name";
        $this->cacheContent = file_exists($this->cacheFile) ? json_decode(file_get_contents($this->cacheFile)) : new stdClass();
    }


    /**
     * getDate
     *
     * Returns the *timestamp*, describing when data was saved to the cache.
     *
     * @param string $name The name of a field
     * @return int|null
     */
    public function getDate($name)
    {
        if ($this->disabled)
            return null;

        return (isset($this->cacheContent->{$name})) ? $this->cacheContent->{$name}['date'] : null;
    }


    /**
     * Magic method __get
     *
     * Lets you get cached data using the standard object syntax.
     *
     * @param string $name The name of a field
     * @return mixed
     */
    public function __get($name)
    {
        if ($this->disabled)
            return null;

        return (isset($this->cacheContent->{$name})) ? $this->cacheContent->{$name}['value'] : null;
    }


    /**
     * Magic method __set
     *
     * Lets you change cached data using the standard object syntax.
     * If the *autosave* mode was enabled, the method also dumps the cache to the disk.
     * If an error occurs while saving the data, a DumpCacheFileError exception will be thrown.
     *
     * @param string $name The name of a field
     * @param mixed $value The value for the field
     * @return void
     * @throws RuntimeException
     */
    public function __set($name, $value)
    {
        $this->cacheContent->{$name} = [
            'value' => $value,
            'date' => time()
        ];

        if ($this->autosave)
            if (!$this->save())
                throw new RuntimeException("Couldn't save the cache to a file!");
    }


    /**
     * Magic method __isset
     *
     * Lets you use isset() and empty() functions.
     *
     * @param string $name The name of a field
     * @return bool
     */
    public function __isset($name)
    {
        if ($this->disabled)
            return false;

        return isset($this->cacheContent->{$name});
    }


    /**
     * Magic method __unset
     *
     * Lets you use unset() function.
     *
     * @param string $name The name of a field
     * @return void
     */
    public function __unset($name)
    {
        unset($this->cacheContent->{$name});
    }


    /**
     * save
     *
     * Dumps the data to the disk.
     *
     * @return bool|int Returns *false* in case of an error. Otherwise, it returns the number of written bytes.
     */
    public function save()
    {
        if ($this->disabled)
            return false;

        return file_put_contents($this->cacheFile, json_encode($this->cacheContent, JSON_UNESCAPED_UNICODE));
    }


    /**
     * disable
     *
     * Lets you temporarily disable the caching system. After the moment of calling this method, all other methods will return *null* or *false* values.
     */
    public function disable()
    {
        $this->disabled = true;
    }


    /**
     * enable
     *
     * Enables the caching system back, after you disable it using the methods `disable()`.
     */
    public function enable()
    {
        $this->disabled = false;
    }


    /**
     * invalidate
     *
     * Deletes the cache file to force its update.
     */
    public function invalidate()
    {
        if (is_dir($this->cacheFile))
            unlink($this->cacheFile);

        $this->cacheContent = new stdClass();
    }
}
