<?php
namespace ru\kozalo\tests;

require("../src/SimpleCache.php");

use ru\kozalo\SimpleCache;

/**
 * Class SimpleCacheTests
 *
 * Test class for the SimpleCache.
 *
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 * @package ru\kozalo\tests
 * @see SimpleCache
 */
class SimpleCacheTests extends \PHPUnit_Framework_TestCase {

    const CACHE_NAME = 'test_cache';

    private function initCache()
    {
        $cache = new SimpleCache(self::CACHE_NAME, false);
        $cache->invalidate();
        $cache->someNumber = 123;
        $cache->someString = "qwerty";
        $cache->save();
        return $cache;
    }

    public function testAccessors()
    {
        $cache = $this->initCache();

        $this->assertEquals($cache->someNumber, 123);
        $this->assertEquals($cache->someString, "qwerty");
    }

    public function testOtherMagicMethods()
    {
        $cache = $this->initCache();
        unset($cache->someNumber);

        $this->assertTrue(empty($cache->someNumber));
        $this->assertTrue(isset($cache->someString));
        $this->assertFalse(isset($cache->someNumber));
        $this->assertFalse(empty($cache->someString));

        $this->assertNull($cache->someNumber);
        $this->assertEquals($cache->someString, "qwerty");
    }

    public function testInvalidate()
    {
        $cache = $this->initCache();
        $cache->invalidate();

        $this->assertNull($cache->someNumber);
        $this->assertNull($cache->someString);
    }

    public function testSwitch()
    {
        $cache = $this->initCache();

        $cache->disable();
        $this->assertNull($cache->someNumber);
        $this->assertNull($cache->someString);

        $cache->enable();
        $this->assertNotNull($cache->someNumber);
        $this->assertNotNull($cache->someString);
    }

    public function testGetDate()
    {
        $cache = $this->initCache();

        $this->assertNotNull($cache->getDate('someNumber'));
        $this->assertNotNull($cache->getDate('someString'));
    }

}